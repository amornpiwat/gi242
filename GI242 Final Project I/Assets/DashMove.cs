﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashMove : MonoBehaviour
{
    Rigidbody2D rb;
    float movX;

    public float DashForce = 1;
    public float StartDashTimer = 0.1f;
    float CurrentDashTimer;
    float DashDirection;

    bool isDashing;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        movX = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown(KeyCode.LeftShift) && movX != 0)
        {
            isDashing = true;
            CurrentDashTimer = StartDashTimer;
            rb.velocity = Vector2.zero;
            DashDirection = (int)movX;
        }

        if (isDashing)
        {
            rb.velocity = transform.right * DashDirection * DashForce;

            CurrentDashTimer -= Time.deltaTime;

            if(CurrentDashTimer <= 0)
            {
                isDashing = false;
            }
        }
    }
}